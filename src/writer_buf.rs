use std::io::Write;
use std::sync::Arc;
use std::sync::Mutex;
#[derive(Clone)]
pub struct WriterBuf(Arc<Mutex<Vec<u8>>>);

impl WriterBuf {
  pub fn new(w: Arc<Mutex<Vec<u8>>>) -> Self {
    WriterBuf(w)
  }

  pub fn buffer_as_parsed_json(&mut self) -> Vec<serde_json::Value> {
    let s = String::from_utf8(self.0.lock().unwrap().to_vec()).unwrap();
    let log_vec: Vec<serde_json::Value> = s
      .split('\n')
      .filter(|l| l.starts_with('{'))
      .map(|l| serde_json::from_str(&l).unwrap())
      .collect();
    log_vec
  }
}

impl Write for WriterBuf {
  fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
    (*self.0.lock().unwrap()).write(buf)
  }
  fn flush(&mut self) -> std::io::Result<()> {
    (*self.0.lock().unwrap()).flush()
  }
}
