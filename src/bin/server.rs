//! API for snark intro, based on rocket.rs
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate slog;
extern crate slog_json;

use rocket::http::Method;

use rocket::{get, routes, State};
use rocket_contrib::json::{Json, JsonValue};
use rocket_cors;
use rocket_cors::Error;
use slog::{Drain, Logger};
use std::sync::Mutex;
use snark_intro;
use snark_intro::prover;
use snark_intro::writer_buf::WriterBuf;
extern crate hex;

use std::panic;
use std::sync::Arc;

// just to check if the server is alive
#[get("/")]
fn index() -> &'static str {
    "bandersnatch"
}

#[derive(Debug, Deserialize)]
struct RawTx {
    raw_tx: String,
}

// tx details and result of verify check with intermediate values
#[post("/tx_details", data = "<raw_tx>")]
fn tx_details(raw_tx: Json<RawTx>, my_tx_provider: State<snark_intro::MyTxProver>) -> JsonValue {
    let tx = snark_intro::load_and_convert_tx(&raw_tx.raw_tx);

    // Instead of a output stream (like stdout), we write all logs to a buffer.
    // This is a messy approach because we use the slog_json logger to write json into a byte buffer
    // and later we have to parse the byte buffer to be able to add the logs to the json! macro to generate the response.
    // For sure there is a more straight forward solution ;-)
    let mut buf = WriterBuf::new(Arc::new(Mutex::new(vec![])));
    let json_drain = Mutex::new(slog_json::Json::default(buf.clone())).map(slog::Fuse);
    let log = Logger::root(json_drain, o!());

    let v = snark_intro::Verifier::new(log, &my_tx_provider);

    let result = v.verify_tx(&snark_intro::load_transaction(&raw_tx.raw_tx));

    json!({ "status": "ok", "tx": tx , "log": buf.buffer_as_parsed_json(), "result": result})
}

// creates a note with given value and returns generated proof
#[get("/create_note_and_proof/<value>")]
fn create_note_and_proof(value: u64, my_tx_provider: State<snark_intro::MyTxProver>) -> JsonValue {
    let pr = prover::generate_proof(value, &my_tx_provider);
    json!({ "proof_results": pr })
}

// verifies given proof and returns result 'true' or error message
#[post("/verify_proof", data = "<verify_data>")]
fn verify_proof(
    verify_data: Json<prover::VerifyData>,
    my_tx_provider: State<snark_intro::MyTxProver>,
) -> JsonValue {
    let result = match prover::verify_proof(&verify_data, &my_tx_provider) {
        Ok(b) => b.to_string(),
        Err(e) => e.to_string(),
    };

    json!({ "verify_result": result })
}

// start http server with simple CORS support and register routes
fn main() -> Result<(), Error> {
    //let allowed_origins = AllowedOrigins::some_exact(&["https://www.acme.com"]);

    // You can also deserialize this
    let cors = rocket_cors::CorsOptions {
        allowed_methods: vec![Method::Get, Method::Post]
            .into_iter()
            .map(From::from)
            .collect(),
        ..Default::default()
    }
    .to_cors()?;

    rocket::ignite()
        .manage(snark_intro::load_params())
        .mount(
            "/",
            routes![index, tx_details, create_note_and_proof, verify_proof],
        )
        .attach(cors)
        .launch();
    Ok(())
}
