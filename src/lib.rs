//! Provides all necessary functions for the server binary
use bellman::groth16::{Parameters, PreparedVerifyingKey};
use group::CurveAffine;
use hex::decode;
use pairing::bls12_381::Bls12;
use slog::Logger;
use std::process;
use std::time::Instant;

use zcash_primitives::consensus::BranchId;
use zcash_primitives::consensus::MainNetwork;
use zcash_primitives::redjubjub::PublicKey;
use zcash_primitives::transaction::Transaction;
use zcash_primitives::transaction::{signature_hash, SIGHASH_ALL};
use zcash_primitives::JUBJUB;

use zcash_proofs::prover::LocalTxProver;
use zcash_proofs::sapling::SaplingVerificationContext;

use bellman::groth16::prepare_verifying_key;
use bellman::groth16::Proof;

#[macro_use]
extern crate slog;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_hex;

pub mod tx_details;
pub mod writer_buf;

pub mod prover;

pub fn load_and_convert_tx(hex_tx: &str) -> tx_details::TxDetails {
  let tx_buffer = decode(hex_tx).unwrap();
  let tx = Transaction::read(&tx_buffer[..]).unwrap();

  tx_details::TxDetails::from(&tx)
}

pub fn load_transaction(hex_tx: &str) -> Transaction {
  let tx_buffer = decode(hex_tx).unwrap();
  let tx = Transaction::read(&tx_buffer[..]).unwrap();

  return tx;
}

///
/// All verifications are available as methods of this struct
///
/// We use logging to collect intermediate values.
///
pub struct Verifier<'a> {
  log: Logger,
  params: &'a MyTxProver,
}

impl Verifier<'_> {
  pub fn new(log: Logger, params: &MyTxProver) -> Verifier {
    Verifier { log, params }
  }

  ///
  /// Verify given transaction, returns the result of the verification
  pub fn verify_tx(&self, tx: &Transaction) -> bool {
    let mut ctx = SaplingVerificationContext::new(self.log.new(o!("sapctx" => "")));

    let mut result = true;
    //
    // check spends
    //
    for (i, spend) in tx.shielded_spends.iter().enumerate() {
      let branch_id = BranchId::for_height::<MainNetwork>(tx.expiry_height);
      let mut sighash = [0u8; 32];
      sighash.copy_from_slice(&signature_hash(&tx, branch_id, SIGHASH_ALL, None));

      let proof_spend = Proof::read(&spend.zkproof[..]).unwrap();

      let b: pairing::bls12_381::G2Affine = proof_spend.b;
      let b = b.into_uncompressed();
      let mut b = hex::encode(b);
      b.truncate(2 * 96); // contains xc1 xc0
      let b_x_c0 = b.split_off(96);
      info!(self.log,"spend {}", i; 
            "proof_a" => ?proof_spend.a, 
            "proof_a_x" =>  g1affine_to_hex_x(&proof_spend.a), 
            "proof_b" => ?proof_spend.b, 
            "proof_b_x_c0" => b_x_c0, 
            "proof_b_x_c1" => b, 
            "proof_c" => ?proof_spend.c, 
            "proof_c_x" => g1affine_to_hex_x(&proof_spend.c));

      let copy_spend_rk = PublicKey(spend.rk.0.clone());
      let r = SaplingVerificationContext::check_spend(
        &mut ctx,
        spend.cv.clone(),
        spend.anchor,
        &spend.nullifier,
        copy_spend_rk,
        &sighash, //sig_hash_value
        spend.spend_auth_sig.unwrap(),
        proof_spend,
        &self.params.spend_vk,
        &JUBJUB,
      );

      info!(self.log,"spend {}", i; "result" => r);

      result &= r;
    }

    // return the x value as hex string
    fn g1affine_to_hex_x(affine: &pairing::bls12_381::G1Affine) -> String {
      let a = affine.into_uncompressed(); // x part
      let mut a = hex::encode(a);
      a.truncate(96);

      a
    }

    //
    // check outputs
    //
    for (i, output) in tx.shielded_outputs.iter().enumerate() {
      let out_verifying_key = prepare_verifying_key(&self.params.output_params.vk);
      let proof_out = Proof::read(&output.zkproof[..]).unwrap(); //new
      let r = SaplingVerificationContext::check_output(
        //new
        &mut ctx,
        output.cv.clone(),
        output.cmu,
        output.ephemeral_key.clone(),
        proof_out,
        &out_verifying_key,
        &JUBJUB,
      );
      info!(self.log,"output {}", i; "result" => r);

      result &= r;
    }

    result
  }
}

/// Access to public parameters
pub struct MyTxProver {
  pub spend_params: Parameters<Bls12>,
  pub spend_vk: PreparedVerifyingKey<Bls12>,
  pub output_params: Parameters<Bls12>,
}
pub fn load_params() -> MyTxProver {
  println!("Start to load parameters ...");
  let now = Instant::now();
  let tx_prover = match LocalTxProver::with_default_location() {
    Some(tx_prover) => tx_prover,
    None => {
      println!("Please run zcash-fetch-params or fetch-params.sh to download the parameters.");
      process::exit(1);
    }
  };

  println!("Finished in {}s.", now.elapsed().as_secs());

  let params_exposed: MyTxProver = unsafe { std::mem::transmute(tx_prover) };
  return params_exposed;
}
