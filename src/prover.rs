//! Provides function for dummy note and proof generation. Notes are newly generated and don't come from any unspent
//! notes pool.
use crate::tx_details::{fr_from_my_fr, my_fr_from_fr, MyFr, TwistedEdwardsPoint};
use crate::MyTxProver;
use bellman::{gadgets::multipack, groth16::Proof};
use ff::{Field, PrimeField};
use group::CurveAffine;
use pairing::bls12_381::{Bls12, Fr};
use std::error::Error;

use rand_core::SeedableRng;
use rand_xorshift::XorShiftRng;
use serde_hex::{impl_serhex_bytearray, SerHex, StrictPfx};
use slog::Logger;

use zcash_primitives::jubjub::fs::Fs;
use zcash_primitives::jubjub::{edwards::Point, Unknown};
use zcash_primitives::transaction::components::GROTH_PROOF_SIZE;
use zcash_primitives::{
    merkle_tree::{CommitmentTree, IncrementalWitness},
    sapling::Node,
    zip32::{ExtendedFullViewingKey, ExtendedSpendingKey},
    JUBJUB,
};
use zcash_proofs::sapling::SaplingProvingContext;


// Needs some extras to support SerHex::<StrictPfx>
type G2ByteArray = [u8; 96];

pub struct G2(pub G2ByteArray);
impl AsRef<[u8]> for G2 {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl From<G2ByteArray> for G2 {
    fn from(b: G2ByteArray) -> G2 {
        G2(b)
    }
}
impl_serhex_bytearray!(G2, 96);

#[derive(Serialize, Deserialize)]
pub struct Proof3Point {
    #[serde(with = "SerHex::<StrictPfx>")]
    pub a: [u8; 48],
    #[serde(with = "SerHex::<StrictPfx>")]
    pub b: G2,
    #[serde(with = "SerHex::<StrictPfx>")]
    pub c: [u8; 48],
}

impl From<Proof<pairing::bls12_381::Bls12>> for Proof3Point {
    fn from(p: Proof<pairing::bls12_381::Bls12>) -> Proof3Point {
        let mut pa_array: [u8; 48] = [0; 48];
        pa_array.copy_from_slice(p.a.into_compressed().as_ref());
        let mut pb_array: [u8; 96] = [0; 96];
        pb_array.copy_from_slice(p.b.into_compressed().as_ref());
        let mut pc_array: [u8; 48] = [0; 48];
        pc_array.copy_from_slice(p.c.into_compressed().as_ref());
        Proof3Point {
            a: pa_array,
            b: G2(pb_array),
            c: pc_array,
        }
    }
}

#[derive(Serialize)]
pub struct ProofResult {
    #[serde(with = "SerHex::<StrictPfx>")]
    cm: MyFr,
    //path: Vec<MyFr>,
    #[serde(with = "SerHex::<StrictPfx>")]
    anchor: MyFr,
    #[serde(with = "SerHex::<StrictPfx>")]
    nf: [u8; 32],
    //#[serde(with = "SerHex::<StrictPfx>")]
    //proof: MyZkProof, // Proof<Bls12>,
    proof: Proof3Point,
    cv: TwistedEdwardsPoint, // edwards::Point<Bls12, Unknown>
    rk: TwistedEdwardsPoint, //  PublicKey<Bls12>
}

pub fn generate_proof(value: u64, params: &MyTxProver) -> ProofResult {
    let extsk = ExtendedSpendingKey::master(&[]);
    let extfvk = ExtendedFullViewingKey::from(&extsk);

    // destination address
    let to = extfvk.default_address().unwrap().1;
    let proof_generation_key = extsk.expsk.proof_generation_key(&JUBJUB);
    //let mut rng = OsRng;

    let mut rng = XorShiftRng::from_seed([
        0x59, 0x62, 0xbe, 0x3d, 0x76, 0x3d, 0x31, 0x8d, 0x17, 0xdb, 0x37, 0x32, 0x54, 0x06, 0xbc,
        0xe5,
    ]);

    // create note & commitment
    let note = to
        .create_note(value, Fs::random(&mut rng), &JUBJUB)
        .unwrap();
    let cm = Node::new(note.cm(&JUBJUB).into_repr());

    // create empty commitment tree and add commitment
    let mut tree = CommitmentTree::new();
    tree.append(cm).unwrap();
    let witness = IncrementalWitness::from_tree(&tree);
    let merkle_path = witness.path().unwrap();

    let anchor = Fr::from(witness.root());

    let mut ctx = SaplingProvingContext::new();
    let alpha = Fs::random(&mut rng);

    let (proof, cv, rk) = SaplingProvingContext::spend_proof(
        &mut ctx,
        proof_generation_key, //proof_generation_key: ProofGenerationKey<Bls12>,
        *to.diversifier(),    //diversifier: Diversifier,
        note.r,               //rcm: Fs,
        alpha,                //ar: Fs,
        note.value,           //value: u64,
        anchor,               //anchor, rt: Fr,
        merkle_path,          //merkle_path: MerklePath<Node>,
        &params.spend_params, //proving_key: &Parameters<Bls12>,
        &params.spend_vk,     //verifying_key: &PreparedVerifyingKey<Bls12>,
        &JUBJUB,              //params: &JubjubBls12,
    )
    .expect("proving should not fail");

    let mut nullifier = [0u8; 32];
    nullifier.copy_from_slice(
        &note.nf(
            &extsk
                .expsk
                .proof_generation_key(&JUBJUB)
                .to_viewing_key(&JUBJUB),
            witness.path().unwrap().position,
            &JUBJUB,
        ),
    );
    ProofResult {
        cm: my_fr_from_fr(&Fr::from(cm)),
        anchor: my_fr_from_fr(&Fr::from(anchor)),
        nf: nullifier,
        proof: Proof3Point::from(proof),
        cv: TwistedEdwardsPoint::from(&cv),
        rk: TwistedEdwardsPoint::from(&rk.0),
    }
}

#[derive(Serialize, Deserialize)]
pub struct VerifyData {
    #[serde(with = "SerHex::<StrictPfx>")]
    anchor: MyFr,
    #[serde(with = "SerHex::<StrictPfx>")]
    nf: [u8; 32],
    proof: Proof3Point,
    cv: TwistedEdwardsPoint, // edwards::Point<Bls12, Unknown>
    rk: TwistedEdwardsPoint, //  PublicKey<Bls12>
}

pub fn verify_proof(input: &VerifyData, params: &MyTxProver) -> Result<bool, Box<dyn Error>> {
    let mut vec = Vec::with_capacity(GROTH_PROOF_SIZE);
    vec.extend_from_slice(&input.proof.a);
    vec.extend_from_slice(&input.proof.b.0);
    vec.extend_from_slice(&input.proof.c);

    let proof = Proof::<Bls12>::read(vec.as_slice())?;

    // Construct public input for circuit
    let mut public_input = [Fr::zero(); 7];
    {
        let rk: Point<Bls12, Unknown> = Point::<Bls12, Unknown>::from(&input.rk);
        let (x, y) = rk.to_xy();
        public_input[0] = x;
        public_input[1] = y;
    }
    {
        let cv: Point<Bls12, Unknown> = Point::<Bls12, Unknown>::from(&input.cv);
        let (x, y) = cv.to_xy();
        public_input[2] = x;
        public_input[3] = y;
    }

    public_input[4] = fr_from_my_fr(&input.anchor)?;

    // Add the nullifier through multiscalar packing
    {
        let nullifier = multipack::bytes_to_bits_le(&input.nf);
        let nullifier = multipack::compute_multipacking::<Bls12>(&nullifier);

        assert_eq!(nullifier.len(), 2);

        public_input[5] = nullifier[0];
        public_input[6] = nullifier[1];
    }

    // Verify the proof
    let result = bellman::groth16::verify_proof(
        &params.spend_vk,
        &proof,
        &public_input[..],
        Logger::root(slog::Discard, o!()),
    )?;
    return Ok(result);
}
