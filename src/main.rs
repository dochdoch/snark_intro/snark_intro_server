#[macro_use]
extern crate slog;
extern crate slog_json;

use bellman::groth16::Proof;
use slog::Drain;

use std::env;
use std::process;
use std::sync::Mutex;

use zcash_primitives::consensus::BranchId;
use zcash_primitives::consensus::MainNetwork;

use bellman::groth16::prepare_verifying_key;
use snark_intro;
use zcash_primitives::redjubjub::PublicKey;
use zcash_primitives::transaction::{signature_hash, SIGHASH_ALL};
use zcash_primitives::JUBJUB;

use zcash_proofs::sapling::SaplingVerificationContext;

// Just a small tool to verify the first spend and output description of a given raw encoded transaction.
// Will fail for transactions that are not shielded.
fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("not enough arguments, expect raw hex transaction as argument");
        process::exit(1);
    }
    let hex_tx = &args[1];

    let tx = snark_intro::load_transaction(hex_tx);

    let params_exposed = snark_intro::load_params();
    let json_drain = Mutex::new(slog_json::Json::default(std::io::stderr())).map(slog::Fuse);
    let log = slog::Logger::root(json_drain, o!());
    let mut ctx = SaplingVerificationContext::new(log);

    //
    // check_spend
    //
    let spend = &tx.shielded_spends[0];

    let branch_id = BranchId::for_height::<MainNetwork>(tx.expiry_height);
    let mut sighash = [0u8; 32];
    sighash.copy_from_slice(&signature_hash(&tx, branch_id, SIGHASH_ALL, None));

    let proof_spend = Proof::read(&spend.zkproof[..]).unwrap();

    let copy_spend_rk = PublicKey(spend.rk.0.clone());
    let result = SaplingVerificationContext::check_spend(
        &mut ctx,
        spend.cv.clone(),
        spend.anchor,
        &spend.nullifier,
        copy_spend_rk,
        &sighash, //sig_hash_value
        spend.spend_auth_sig.unwrap(),
        proof_spend,
        &params_exposed.spend_vk,
        &JUBJUB,
    );

    println!("Result check_spend: {}", result);
    //
    // check_output
    //
    let output = &tx.shielded_outputs[0];
    let out_verifying_key = prepare_verifying_key(&params_exposed.output_params.vk);
    let proof_out = Proof::read(&output.zkproof[..]).unwrap(); //new
    let result = SaplingVerificationContext::check_output(
        //new
        &mut ctx,
        output.cv.clone(),
        output.cmu,
        output.ephemeral_key.clone(),
        proof_out,
        &out_verifying_key,
        &JUBJUB,
    );
    println!("Result check_output: {}", result);
}
