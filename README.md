API backend for [SNARK Intro](https://doch_doch.gitlab.io/snark_intro/snark_intro_front/).

## Dependencies

For faster development in the forked librustzcash crates, snark_intro_server depends on a local version of the logging enabled librustzcash.
Execute `git clone https://github.com/adler99/librustzcash` in the parent directory of your snark_intro_server. It should look like
```
somewhere/snark_intro_server
somewhere/librustzcash
```

You can change that in `Cargo.toml`.

## Setup

Run `sh ./fetch-params.sh` to download parameters.

## Usage


### Server

```shell
cargo run --release --bin server
```

### Verifier

Playground to verify a given raw transaction.

```shell
cargo run --bin snark_intro `cat tx_data/shielded_tx.hex`
```
This will use hex encoded transaction data stored in file `tx_data/shielded_tx.hex`. Tx needs to have a spend description.